/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package liga2;

import java.util.List;
import java.util.Set;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import static org.hibernate.criterion.Order.asc;
import static org.hibernate.criterion.Order.desc;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Lenovo
 */
public class DatabaseAccessObject {
    
    public static boolean addKlub(String nazwa, String stadion, String trener, int goleStrzelone, int goleStracone, int punkty)
    {
        int klubId = 0;
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Klub klub = new Klub(nazwa,stadion,trener,goleStrzelone,goleStracone,punkty);
            klubId = (Integer) session.save(klub);
            tx.commit();
        }catch(Exception e){
            System.out.println(e.getMessage());
            if(tx != null){
                tx.rollback();
            }
        }finally{
            session.close();
        }
        
        return klubId > 0;
    }
    
    public static boolean addMecz(Klub klubByIdGospodarz, Klub klubByIdGosc, int kolejka)
    {
        int meczId = 0;
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Mecz mecz = new Mecz(klubByIdGospodarz, klubByIdGosc, kolejka);
            meczId = (Integer) session.save(mecz);
            tx.commit();
        }catch(Exception e){
            System.out.println(e.getMessage());
            if(tx != null){
                tx.rollback();
            }
        }finally{
            session.close();
        }
        
        return meczId > 0;
    }
        
    public static ObservableList<Klub> getKlub()
    {
        ObservableList<Klub> klub = null;
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Klub.class);
            cr.addOrder(desc("punkty"));
            cr.addOrder(desc("goleStrzelone"));
            klub = FXCollections.observableArrayList(cr.list());
            tx.commit();
        }catch(Exception e){
            
        }finally{
            session.close();
        }
        
        return klub;
    }
    
        public static ObservableList<Klub> getKlubCrit()
    {
        ObservableList<Klub> klub = null;
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Klub.class);
            klub = FXCollections.observableArrayList(cr.list());
            tx.commit();
        }catch(Exception e){
            
        }finally{
            session.close();
        }
        
        return klub;
    }
    
    public static ObservableList<Klub> getKlub(String team)
    {
        ObservableList<Klub> klub = null;
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Klub.class);
            cr.add(Restrictions.eq("nazwa", team));
            klub = FXCollections.observableArrayList(cr.list());
            tx.commit();
        }catch(Exception e){
            
        }finally{
            session.close();
        }
        
        return klub;
    }
    
    public static List<Mecz> getMecz()
    {
        List<Mecz> mecz = null;
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Mecz.class);
            //cr.addOrder(desc("punkty"));
            mecz = cr.list();
            tx.commit();
        }catch(Exception e){
            
        }finally{
            session.close();
        }
        
        return mecz;
    }
    
    public static ObservableList<Mecz> getMecz(String team)
    {
        ObservableList<Mecz> mecz = null;
        ObservableList<Klub> klub = DatabaseAccessObject.getKlub(team);
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Mecz.class);
            Criterion gosp = Restrictions.in("klubByIdGospodarz", klub);
            Criterion gosc = Restrictions.in("klubByIdGosc", klub);
            cr.add(Restrictions.or(gosc, gosp));
            mecz = FXCollections.observableArrayList(cr.list());
            tx.commit();
        }catch(Exception e){
            
        }finally{
            session.close();
        }
        
        return mecz;
    }
    
    public static ObservableList<Mecz> getKolejka(int kolejka)
    {
        ObservableList<Mecz> mecz = null;
        //ObservableList<Klub> kluby = DatabaseAccessObject.getKlub(team);
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Mecz.class);
            cr.add(Restrictions.eq("kolejka", kolejka));
            mecz = FXCollections.observableArrayList(cr.list());
            tx.commit();
        }catch(Exception e){
            
        }finally{
            session.close();
        }
        
        return mecz;
    }
    
    public static void punktyKlub(Klub klub, int punkty)
    {
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        int punkty1 = klub.getPunkty();
        try{
            tx = session.beginTransaction();
            klub.setPunkty(punkty1 + punkty);
            session.update(klub);
            tx.commit();
        }catch(Exception e){
            
        }finally{
            session.close();
        }
    }
    
    public static void updateTrenerKlub(String klub, String trener)
    {
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        ObservableList<Klub> kluby = DatabaseAccessObject.getKlub(klub);
        Transaction tx = null;
        try{
            Klub k = kluby.get(0);
            tx = session.beginTransaction();
            k.setTrener(trener);
            session.update(k);
            tx.commit();
        }catch(Exception e){
            
        }finally{
            session.close();
        }
    }
    
    public static void updateStadionKlub(String klub, String stadion)
    {
        ObservableList<Klub> kluby = DatabaseAccessObject.getKlub(klub);
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Klub k = kluby.get(0);
            k.setStadion(stadion);
            session.update(k);
            tx.commit();
            
        }catch(Exception e){
            
        }finally{
            session.close();
        }
    }
    
    public static void updateGol(Mecz mecz, int goleGospodarz, int goleGosc)
    {
        List<Mecz> m = DatabaseAccessObject.getMecz();
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            mecz.setGoleGospodarz(goleGospodarz);
            mecz.setGoleGosc(goleGosc);
            session.update(mecz);
            tx.commit();
            
        }catch(Exception e){
            
        }finally{
            session.close();
        }
    }
    
    public static void bilansKlub(Klub klub, int dodatnie, int ujemne)
    {
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        int strzelone = klub.getGoleStrzelone();
        int stracone = klub.getGoleStracone();
        try{
            tx = session.beginTransaction();
            klub.setGoleStrzelone(dodatnie + strzelone);
            klub.setGoleStracone(ujemne + stracone);
            session.update(klub);
            tx.commit();
        }catch(Exception e){
            
        }finally{
            session.close();
        }
    }
}
