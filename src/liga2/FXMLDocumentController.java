/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package liga2;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import com.sun.glass.ui.Window;
import static java.lang.Integer.parseInt;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import org.hibernate.Session;
import static org.hibernate.internal.util.collections.CollectionHelper.isEmpty;

/**
 *
 * @author Lenovo
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private TableView<Klub> tableView;
    @FXML
    private TableColumn<Klub,String>nazwa;
    @FXML
    private TableColumn<Klub,Integer>punkty;
    @FXML
    private TableColumn<Klub,Integer>goleStrzelone;
    @FXML
    private TableColumn<Klub,Integer>goleStracone;
    @FXML
    private TableView<Mecz> tableView2;
    @FXML
    private TableColumn<Mecz,Klub>klubByIdGospodarz;
    @FXML
    private TableColumn<Mecz,Klub>klubByIdGosc;
    @FXML
    private TableColumn<Mecz,Integer>goleGospodarz;
    @FXML
    private TableColumn<Mecz,Integer>goleGosc;
    @FXML
    private TableView<Klub> tableView3;
    @FXML
    private TableColumn<Klub,String>klubCol;
    @FXML
    private TableColumn<Klub,String>trenerCol;
    @FXML
    private TableColumn<Klub,String>stadionCol;
    @FXML
    ComboBox combobox;
    @FXML
    ComboBox comboboxUpdate;
    @FXML
    private TextField trener = new TextField();
    @FXML
    private TextField stadion = new TextField();
    @FXML
    private Label gospl1 = new Label();
    @FXML
    private Label goscl1 = new Label();
    @FXML
    private TextField gospw1 = new TextField();
    @FXML
    private TextField goscw1 = new TextField();
    @FXML
    private Label gospl2 = new Label();
    @FXML
    private Label goscl2 = new Label();
    @FXML
    private TextField gospw2 = new TextField();
    @FXML
    private TextField goscw2 = new TextField();
    @FXML
    private Label gospl3 = new Label();
    @FXML
    private Label goscl3 = new Label();
    @FXML
    private TextField gospw3 = new TextField();
    @FXML
    private TextField goscw3 = new TextField();
    @FXML
    private Label gospl4 = new Label();
    @FXML
    private Label goscl4 = new Label();
    @FXML
    private TextField gospw4 = new TextField();
    @FXML
    private TextField goscw4 = new TextField();
    @FXML
    private Label gospl5 = new Label();
    @FXML
    private Label goscl5 = new Label();
    @FXML
    private TextField gospw5 = new TextField();
    @FXML
    private TextField goscw5 = new TextField();
    @FXML
    private Label gospl6 = new Label();
    @FXML
    private Label goscl6 = new Label();
    @FXML
    private TextField gospw6 = new TextField();
    @FXML
    private TextField goscw6 = new TextField();
    @FXML
    private Label gospl7 = new Label();
    @FXML
    private Label goscl7 = new Label();
    @FXML
    private TextField gospw7 = new TextField();
    @FXML
    private TextField goscw7 = new TextField();
    @FXML
    private Label gospl8 = new Label();
    @FXML
    private Label goscl8 = new Label();
    @FXML
    private TextField gospw8 = new TextField();
    @FXML
    private TextField goscw8 = new TextField();
    @FXML
    private Button btn = new Button();
    
    ArrayList<TextField> poleGosp = new ArrayList();
    ArrayList<TextField> poleGosc = new ArrayList();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<Klub> kluby = DatabaseAccessObject.getKlub();
        nazwa.setCellValueFactory(new PropertyValueFactory<Klub,String>("nazwa"));
        punkty.setCellValueFactory(new PropertyValueFactory<Klub,Integer>("punkty"));
        goleStrzelone.setCellValueFactory(new PropertyValueFactory<Klub,Integer>("goleStracone"));
        goleStracone.setCellValueFactory(new PropertyValueFactory<Klub,Integer>("goleStrzelone"));
        tableView.setItems(kluby);
        ObservableList<Klub> kluby2 = DatabaseAccessObject.getKlubCrit();
        for(Klub klub:kluby2){
            combobox.getItems().add(klub.getNazwa());
            comboboxUpdate.getItems().add(klub.getNazwa());
        }
    }

    @FXML
    public void closeApp(ActionEvent e){
        Platform.exit();
        System.exit(0);
    }
    
    @FXML
    public void addTeam(ActionEvent e)
    {
        DatabaseAccessObject.addKlub("Arka Gdynia", "Stadion Miejski w Gdyni", "Zbigniew Smółka", 0, 0, 0);
        DatabaseAccessObject.addKlub("Cracovia", "Stadion Cracovii im. Józefa Piłsudskiego", "Michał Probierz", 0, 0, 0);
        DatabaseAccessObject.addKlub("Górnik Zabrze", "Stadion im. Ernesta Pohla", "Marcin Brosz", 0, 0, 0);
        DatabaseAccessObject.addKlub("Jagiellonia Białystok", "Stadion Miejski w Białymstoku", "Ireneusz Mamrot", 0, 0, 0);
        DatabaseAccessObject.addKlub("KGHM Zagłębie Lubin", "Dialog Arena", "Ben van Dael", 0, 0, 0);
        DatabaseAccessObject.addKlub("Korona Kielce", "Suzuki Arena", "Gino Lettieri", 0, 0, 0);
        DatabaseAccessObject.addKlub("Lech Poznań", "Inea Stadion", "Adam Nawałka", 0, 0, 0);
        DatabaseAccessObject.addKlub("Lechia Gdańsk", "Stadion Energa Gdańsk", "Piotr Stokowiec", 0, 0, 0);
        DatabaseAccessObject.addKlub("Legia Warszawa", "Stadion Wojska Polskiego", "Ricardo Sa Pinto", 0, 0, 0);
        DatabaseAccessObject.addKlub("Miedź Legnica", "Stadion Miejski w Legnicy", "Dominik Nowak", 0, 0, 0);
        DatabaseAccessObject.addKlub("Piast Gliwice", "Stadion Miejski w Gliwicach", "Waldemar Fornalik", 0, 0, 0);
        DatabaseAccessObject.addKlub("Pogoń Szczecin", "Stadion im. Floriana Krygiera", "Kosta Runajić", 0, 0, 0);
        DatabaseAccessObject.addKlub("Śląsk Wrocław", "Stadion Miejski we Wrocławiu", "Tadeusz Pawłowski", 0, 0, 0);
        DatabaseAccessObject.addKlub("Wisła Kraków", "Stadion Miejski w Krakowie", "Maciej Stolarczyk", 0, 0, 0);
        DatabaseAccessObject.addKlub("Wisła Płock", "Stadion im. Kazimierza Górskiego", "Jose Antonio Vicuna", 0, 0, 0);
        DatabaseAccessObject.addKlub("Zagłębie Sosnowiec", "Stadion Ludowy", "Vadas Ivanauskas", 0, 0, 0);
    }
    
    @FXML
    public void addKolejka(ActionEvent e)
    {
        Random r = new Random();
        List<Mecz> m = DatabaseAccessObject.getMecz();
        List<Integer> kol = new ArrayList();
        List<Klub> kluby = DatabaseAccessObject.getKlub();
        for(int j = 0;j < m.size();j++){
            kol.add(m.get(j).getKolejka());
        }
        if(kol.isEmpty()){
            kol.add(0);
        }
        
        List<Integer> los = new ArrayList();
        for(Integer z = 0;z < kluby.size();z++)
        {
            los.add(z);
        }
            
        int i = 0;
        while(i != 8)
        {
            List<Mecz> mecz = DatabaseAccessObject.getMecz();
            int zakres = los.size();
            Integer gosc = los.get(r.nextInt(zakres));
            Integer gospodarz = los.get(r.nextInt(zakres));
            Klub klubByIdGospodarz;
            Klub klubByIdGosc;
            klubByIdGospodarz = kluby.get(gospodarz);
            klubByIdGosc = kluby.get(gosc);
            int k = getMax(kol) + 1;
            boolean czy = true;
            for(int j = 0;j < mecz.size();j++){
                if((klubByIdGospodarz.getId().intValue() == mecz.get(j).getKlubByIdGospodarz().getId().intValue()) && (klubByIdGosc.getId().intValue() == mecz.get(j).getKlubByIdGosc().getId().intValue()))
                {
                    czy = false;
                }
                if(klubByIdGospodarz.getId().intValue() == klubByIdGosc.getId().intValue()){
                    czy = false;
                }
            }
            
            if(czy == true){
                DatabaseAccessObject.addMecz(klubByIdGospodarz, klubByIdGosc, k);
                los.remove(gospodarz);
                los.remove(gosc);
                i++;
            }
        }
        wynik(getMax(kol) + 1);
        /*ObservableList<Klub> tabela = DatabaseAccessObject.getKlub();
        nazwa.setCellValueFactory(new PropertyValueFactory<Klub,String>("nazwa"));
        punkty.setCellValueFactory(new PropertyValueFactory<Klub,Integer>("punkty"));
        goleStrzelone.setCellValueFactory(new PropertyValueFactory<Klub,Integer>("goleStracone"));
        goleStracone.setCellValueFactory(new PropertyValueFactory<Klub,Integer>("goleStrzelone"));
        tableView.setItems(tabela);*/
    }
    
    public void wynik(int k)
    {
        List<Mecz> m = DatabaseAccessObject.getKolejka(k);
            gospl1.setText(m.get(0).getKlubByIdGospodarz().getNazwa());
            goscl1.setText(m.get(0).getKlubByIdGosc().getNazwa());
            gospl2.setText(m.get(1).getKlubByIdGospodarz().getNazwa());
            goscl2.setText(m.get(1).getKlubByIdGosc().getNazwa());
            gospl3.setText(m.get(2).getKlubByIdGospodarz().getNazwa());
            goscl3.setText(m.get(2).getKlubByIdGosc().getNazwa());
            gospl4.setText(m.get(3).getKlubByIdGospodarz().getNazwa());
            goscl4.setText(m.get(3).getKlubByIdGosc().getNazwa());
            gospl5.setText(m.get(4).getKlubByIdGospodarz().getNazwa());
            goscl5.setText(m.get(4).getKlubByIdGosc().getNazwa());
            gospl6.setText(m.get(5).getKlubByIdGospodarz().getNazwa());
            goscl6.setText(m.get(5).getKlubByIdGosc().getNazwa());
            gospl7.setText(m.get(6).getKlubByIdGospodarz().getNazwa());
            goscl7.setText(m.get(6).getKlubByIdGosc().getNazwa());
            gospl8.setText(m.get(7).getKlubByIdGospodarz().getNazwa());
            goscl8.setText(m.get(7).getKlubByIdGosc().getNazwa());
            poleGosp.add(gospw1);
            poleGosp.add(gospw2);
            poleGosp.add(gospw3);
            poleGosp.add(gospw4);
            poleGosp.add(gospw5);
            poleGosp.add(gospw6);
            poleGosp.add(gospw7);
            poleGosp.add(gospw8);
            poleGosc.add(goscw1);
            poleGosc.add(goscw2);
            poleGosc.add(goscw3);
            poleGosc.add(goscw4);
            poleGosc.add(goscw5);
            poleGosc.add(goscw6);
            poleGosc.add(goscw7);
            poleGosc.add(goscw8);

        
        btn.setOnAction(ev -> {
        for(int i = 0; i < 8; i++)
        {
            int goleGospodarz = gospWynik(i);
            int goleGosc = goscWynik(i);
            DatabaseAccessObject.updateGol(m.get(i), goleGospodarz, goleGosc);
            DatabaseAccessObject.bilansKlub(m.get(i).getKlubByIdGospodarz() ,goleGospodarz, goleGosc);
            DatabaseAccessObject.bilansKlub(m.get(i).getKlubByIdGosc() , goleGosc, goleGospodarz);
            if(goleGospodarz > goleGosc)
            {
                DatabaseAccessObject.punktyKlub(m.get(i).getKlubByIdGospodarz() , 3);
            }

            if(goleGospodarz == goleGosc)
            {
                DatabaseAccessObject.punktyKlub(m.get(i).getKlubByIdGospodarz() , 1);
                DatabaseAccessObject.punktyKlub(m.get(i).getKlubByIdGosc() , 1);
            }

            if(goleGosc > goleGospodarz)
            {
                DatabaseAccessObject.punktyKlub(m.get(i).getKlubByIdGosc() , 3);
            }
        }
        ObservableList<Klub> kluby = DatabaseAccessObject.getKlub();
        nazwa.setCellValueFactory(new PropertyValueFactory<Klub,String>("nazwa"));
        punkty.setCellValueFactory(new PropertyValueFactory<Klub,Integer>("punkty"));
        goleStrzelone.setCellValueFactory(new PropertyValueFactory<Klub,Integer>("goleStracone"));
        goleStracone.setCellValueFactory(new PropertyValueFactory<Klub,Integer>("goleStrzelone"));
        tableView.setItems(kluby);
    });
    }
    
    public int gospWynik(int i)
    {
        return Integer.parseInt(poleGosp.get(i).getText());
    }
    
    public int goscWynik(int i)
    {
        return Integer.parseInt(poleGosc.get(i).getText());
    }
    
    @FXML
    public void showTable(ActionEvent e)
    {
        ObservableList<Klub> kluby = DatabaseAccessObject.getKlub();
        nazwa.setCellValueFactory(new PropertyValueFactory<Klub,String>("nazwa"));
        punkty.setCellValueFactory(new PropertyValueFactory<Klub,Integer>("punkty"));
        goleStrzelone.setCellValueFactory(new PropertyValueFactory<Klub,Integer>("goleStracone"));
        goleStracone.setCellValueFactory(new PropertyValueFactory<Klub,Integer>("goleStrzelone"));
        tableView.setItems(kluby);
    }
    
    public int getMax(List<Integer> list){
    int max = Integer.MIN_VALUE;
    for(int i=0; i<list.size(); i++){
        if(list.get(i) > max){
            max = list.get(i);
        }
    }
    return max;
    }
    
    @FXML
    public void meczeDruzyny(ActionEvent e)
    {
        String lista = combobox.getValue().toString();
        ObservableList<Mecz> mecze = DatabaseAccessObject.getMecz(lista);
        klubByIdGospodarz.setCellValueFactory(new PropertyValueFactory<Mecz,Klub>("klubByIdGospodarz"));
        klubByIdGosc.setCellValueFactory(new PropertyValueFactory<Mecz,Klub>("klubByIdGosc"));
        goleGospodarz.setCellValueFactory(new PropertyValueFactory<Mecz,Integer>("goleGospodarz"));
        goleGosc.setCellValueFactory(new PropertyValueFactory<Mecz,Integer>("goleGosc"));
        tableView2.setItems(mecze);
    }
    
    @FXML
    public void wyswietlKlub(ActionEvent e)
    {
        String lista = comboboxUpdate.getValue().toString();
        ObservableList<Klub> klubs = DatabaseAccessObject.getKlub(lista);
        klubCol.setCellValueFactory(new PropertyValueFactory<Klub,String>("nazwa"));
        trenerCol.setCellValueFactory(new PropertyValueFactory<Klub,String>("trener"));
        stadionCol.setCellValueFactory(new PropertyValueFactory<Klub,String>("stadion"));
        tableView3.setItems(klubs);
        DatabaseAccessObject.updateTrenerKlub(lista, trener.getText());
    }
    
    @FXML
    public void zaktualizujKlub(ActionEvent e)
    {
        String lista = comboboxUpdate.getValue().toString();
        String t = trener.getText();
        String s = stadion.getText();
        if(!s.isEmpty()){
            System.out.println("Stadion " + stadion.getText().toString());
            DatabaseAccessObject.updateStadionKlub(lista, stadion.getText());
        }
        if(!t.isEmpty()){
            System.out.println("Trener " + trener.getText().toString());
            DatabaseAccessObject.updateTrenerKlub(lista, trener.getText());
        }
    }
}
